import React from 'react';
import {render} from 'react-dom';
import Post from '@models/Post';
import '@/styles/styles.css';
import json from '@/assets/json.json';
import WebpackLogo from '@/assets/webpack-logo.png';
import './styles/less.less';
import './styles/scss.scss';
import '@/babel.js';

const post = new Post('webpack post title', WebpackLogo);

const App = () => (
    <div className="container">
        <h1>Webpack 5f</h1>
        <div className="logo">
            <img src="./assets/webpack-logo.png" alt="" />
        </div>
        <div className="box">
            <h2>Less</h2>
        </div>
        <div className="card">
            <h2>SCSS</h2>
        </div>
    </div>
);

render(<App/>, document.getElementById('app'))

console.log(post);
console.log(post.toString());
console.log(json);
